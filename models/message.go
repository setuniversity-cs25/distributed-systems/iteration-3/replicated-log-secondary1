package models

import (
	"log"
	"sort"
	"time"
)

type Message struct {
	ID        int       `json:"id"`
	Message   string    `json:"message"`
	CreatedAt time.Time `json:"created_at"`
}

var msgMemory []Message

func InitMsgMemory() {
	msgMemory = make([]Message, 0)

	log.Println("In-memory storage was initialised.")
}

func CreateMessage(message Message) Message {
	// logic for deduplication of messages
	for _, storedMsg := range msgMemory {
		if storedMsg.ID == message.ID {
			return storedMsg
		}
	}

	msgMemory = append(msgMemory, message)

	// sorting for remaining order of messages created
	sort.Slice(msgMemory, func(i, j int) bool {
		return msgMemory[i].ID < msgMemory[j].ID
	})

	log.Println("Data saved in in-memory storage.")

	return msgMemory[len(msgMemory)-1]
}

func GetAllMessages() []Message {
	log.Println("Data receiving from in-memory storage.")

	// In case if some messages are missed, return only those that are total ordered
	numOfMsgStored := len(msgMemory)
	if numOfMsgStored != 0 && numOfMsgStored != msgMemory[numOfMsgStored-1].ID {
		msgToReturn := make([]Message, 0)

		for index, storedMsg := range msgMemory {
			if index == storedMsg.ID-1 {
				msgToReturn = append(msgToReturn, storedMsg)
			}
		}
		return msgToReturn
	}

	return msgMemory
}
