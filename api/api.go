package api

import "github.com/gin-gonic/gin"

func Init(r *gin.Engine) {

	apiRouter := r.Group("/api/replicated-log/v1")

	health := apiRouter.Group("health")
	health.GET("", getHealthStatus)

	messageInMemory := apiRouter.Group("message-memory")
	messageInMemory.GET("", messageInMemoryGetAll)
	messageInMemory.POST("", messageInMemoryCreate)

}
