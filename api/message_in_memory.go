package api

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/setuniversity-cs25/distributed-systems/replicated-log/models"
)

func messageInMemoryGetAll(c *gin.Context) {
	storedMessages := models.GetAllMessages()

	c.JSON(http.StatusOK, storedMessages)
}

func messageInMemoryCreate(c *gin.Context) {
	log.Println("Secondary 1 storing message started")
	var message models.Message
	if err := c.ShouldBindJSON(&message); err != nil {
		log.Println("error occured on parsing in messageInMemoryCreate")
		c.Error(err)
		return
	}

	createdMessage := models.CreateMessage(message)
	log.Println("Secondary 1 message stored")

	log.Println("Secondary 1 storing message finished")

	// Random status to return for experiments
	//
	// statuses := []int{http.StatusCreated, http.StatusInternalServerError}
	// rand.Shuffle(len(statuses), func(i, j int) {
	// 	statuses[i], statuses[j] = statuses[j], statuses[i]
	// })
	// log.Printf("Secondary 1 returns random status: %d\nReal status is: %d", statuses[0], http.StatusCreated)
	// c.JSON(statuses[0], createdMessage)
	c.JSON(http.StatusCreated, createdMessage)
}
