package api

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type healthOption string

type HealthStatus struct {
	Info      string       `json:"info"`
	Status    healthOption `json:"status"`
	Timestamp time.Time    `json:"timestamp"`
}

const (
	healthy healthOption = "Healthy"
)

func getHealthStatus(c *gin.Context) {
	healthStatus := HealthStatus{
		Info:      "Replicated Log Secondary 1",
		Status:    healthy,
		Timestamp: time.Now(),
	}

	c.JSON(http.StatusOK, healthStatus)
}
